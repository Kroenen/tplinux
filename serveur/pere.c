#define CLEF_AUTH_REQUETES         0x00012345
#define CLEF_AUTH_REPONSES         0x00012346

//Le père s'occupe de recevoir les nouveau client, de sotcker leur id et de leur valider l'acces
void pere(int pid){
	int res, statu_auth=0, decalage=0;
	int auth_requete, auth_reponse, zone_reponse1, zone_reponse2;
	listUsers users[20];
	nbUser nb;
	void *mem_partagee1, *mem_partagee2; 

	nb.user=0;

  auth_requete = msgget(CLEF_AUTH_REQUETES, 0700 | IPC_CREAT);
  if (auth_requete == -1) { printf("Error : AUTH requete\n"); }

  auth_reponse = msgget(CLEF_AUTH_REPONSES, 0700 | IPC_CREAT);
  if (auth_reponse == -1) { printf("Error : AUTH reponse\n"); }

  zone_reponse1 =  shmget(CLEF_PRIRAGE_LIST,sizeof(listUsers),0700 | IPC_CREAT);
  if (zone_reponse1 == -1) { printf("Error : zone reponse\n"); }

	if((mem_partagee1 = shmat(zone_reponse1,NULL,0)) == (void*)-1){
  	printf("erreur de lecture \n");
  }

  zone_reponse2 =  shmget(CLEF_PRIRAGE_NB,sizeof(nbUser),0700 | IPC_CREAT);
  if (zone_reponse2 == -1) { printf("Error : zone reponse\n"); }

	if((mem_partagee2 = shmat(zone_reponse2,NULL,0)) == (void*)-1){
  	printf("erreur de lecture \n");
  }

  // Il attend une sollicitation
  do{
	  printf("SERVEUR : Attente d'une sollicitation ...\n");

	  res = msgrcv(auth_requete, &auth, sizeof(auth), 0, 0);
	  if (res == -1) { printf("Error : AUTH Recieve \n"); }

	  // Si l'utilisateur a envoyer un formulaire d'authentification.
	  if(auth.action==0){

		  printf("SERVEUR : %s demande l'authorisation d'entrer dans le tchat.\n",auth.pseudo);

		  // Je ne fais pas de vérification, l'id de l'utilisateur est = au precssus 
		  // Donc il est forcement unique.
		  users[nb.user].id =  auth.id;
		  strcpy(users[nb.user].pseudo, auth.pseudo);

		  ((listUsers*)mem_partagee1)[nb.user] = users[nb.user];

		  nb.user = nb.user+1;

		  *((nbUser*)mem_partagee2) = nb;
		  
		  statu_auth=1;
		  res = msgsnd(auth_reponse, & statu_auth,sizeof(int),0);
		  if (res == -1) { printf("Error : AUTH Send \n"); }

		  statu_auth=0;

	  }else{ // Sinnon le formulaire nous signal qu'il veut se deco.

	  	printf("SERVEUR : %s est parti ... \n", auth.pseudo);

	  	//supprimer l'element de la liste pour ne plus l'averti des modifications.
	  	//je récupère le nombre d'utilisateur
	  	//je récupère l'id a supprimer
	  	decalage = 0;
	  	for (int i = 0; i < nb.user; ++i)
	  	{
	  		users[i] = ((listUsers*)mem_partagee1)[i+decalage]; 
	  		if(users[i].id==auth.id){
	  			users[i] = ((listUsers*)mem_partagee1)[i+1]; 	
	  			decalage++;
	  			nb.user--;
	  		};
	  	}

	  	// Une fois l'utilisateur supprimer de la liste, j'enregistre.
	  	for (int i = 0; i < nb.user; ++i)
	  	{
	  		((listUsers*)mem_partagee1)[i]=users[i];
	  	}

		  *((nbUser*)mem_partagee2) = nb;
	  }


  }while(TRUE);
}