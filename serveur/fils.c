#define CLEF_MESS_REQUETES         0x00012347
#define CLEF_MESS_REPONSES         0x00012348

void alert_client();

//	Le fils reçois les messages des client
//	Il va devoir les mettre en mémoire, et avertir chaque utilisateur que des nouveau messages sont arrivé.
void fils(){

	// 	J'instancie une structure messForm et je l'appel mesg.
	messForm mesg, memsg;

	int res; //		Variable contenant la réponse
	int mess_requete, zone_reponse; 
	int i = 0;
	void* mem_partagee; 

	// Je crée un enplacement pour recevoir les messages
  mess_requete = msgget(CLEF_MESS_REQUETES, 0700 | IPC_CREAT);
  if (mess_requete == -1) { printf("Error : MESS requete\n"); }

  //	Je crée un emplacement pour stocker les messages
  zone_reponse =  shmget(CLEF_MESS_REPONSES,sizeof(messForm),0700 | IPC_CREAT);
  if (zone_reponse == -1) { printf("Error : zone reponse\n"); }

  // Il attend qu'un utilisateur envoi un message.
  do{

  	res = msgrcv(mess_requete, & mesg, sizeof(mesg),0,0);
	  if (res == -1) { printf("Error : MESS Recieve 1\n"); }

	  // Si l'id est a 0, cela signifie que le message n'est pas destiné a quelqu'un en particuler,
	  // on vas donc l'envoyer a tout le monde
	  if(mesg.id==0){
		  printf("TCHAT: %s a envoyé : %s \n", mesg.pseudo, mesg.content);

		  if((mem_partagee = shmat(zone_reponse,NULL,0)) == (void*)-1){
		  	printf("erreur de lecture \n");
		  }

		  //Ont stock le message dans une memoire partgée
		  memsg = *((messForm*)mem_partagee);
		  *((messForm*)mem_partagee) = mesg;

		  // J'averti les clients qu'il on un message a recupéré.
		  alert_client();

	  }else{	//	Sinnon, le message est destiner a quelqu'un.

	  	printf("Private: %s a envoyé un message privé\n",mesg.pseudo);

	  	// L'ID  correspond au pid du client,
	  	// je peut donc directement envoyer le message dans la boite au lettre.
	  	res = msgsnd(msgget(mesg.id, 0700 | IPC_CREAT),&mesg,sizeof(mesg),0);
		  if (res == -1) { printf("Error : AUTH Send \n"); }
	  }

	  // je n'ai pas de signal de sorti
  }while(TRUE);
}

//	Cette fonction est appeler pour signaler au client qu'il ont a message a lire.
//>> Pour la rendre plus intelligente, il  faudrais lui fournir l'id de l'emetteur pour ne pas lui envoyer de signal de lecture
//	Ou lui envoyer un signal different.
void alert_client()
{
	int zone_reponse1, zone_reponse2;
	listUsers users[20];
	nbUser nb;
	void *mem_partagee1, *mem_partagee2;

	//	Je récupère la liste des client qui sont connu par le serveur.
  zone_reponse1 =  shmget(CLEF_PRIRAGE_LIST,sizeof(listUsers),0700 | IPC_CREAT);
  if (zone_reponse1 == -1) { printf("Error : zone reponse 2\n"); }

	if((mem_partagee1 = shmat(zone_reponse1,NULL,0)) == (void*)-1){
  	printf("erreur de lecture \n");
  }

	zone_reponse2 =  shmget(CLEF_PRIRAGE_NB,sizeof(nbUser),0700 | IPC_CREAT);
  if (zone_reponse2 == -1) { printf("Error : zone reponse 3\n"); }

	if((mem_partagee2 = shmat(zone_reponse2,NULL,0)) == (void*)-1){
  	printf("erreur de lecture \n");
  }

  nb = *((nbUser*)mem_partagee2);

  // J'envoi un signal a tout le monde.
  for (int i = 0; i < nb.user; ++i)
  {
	  users[i] = ((listUsers*)mem_partagee1)[i];  	
	  kill(users[i].id,SIGUSR1);
  }
}