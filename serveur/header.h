#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <signal.h>
 
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>

#define TRUE (1==1)
#define FALSE (!TRUE) 
#define LG_MAX                512
#define CLEF_PRIRAGE_LIST			 0x00012349
#define CLEF_PRIRAGE_NB			   0x00012350

typedef struct{
	long id;
	char pseudo[ LG_MAX ];	
}listUsers;

typedef struct 
{
	int user;
} nbUser;
	
//	Je crée un structure pour contenir un message
typedef struct {
	int id;
	char pseudo[ LG_MAX ];
	char content[ LG_MAX ];
} messForm;

struct authForm
{
	long id;
	int action;
	char pseudo[ LG_MAX ];
} auth;


void new_mess_tchat(int sig);