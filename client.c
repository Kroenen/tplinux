#include "client/header.h"
#include "client/saisie_tchat.c"
#include "client/signal_tchat.c"


void main(void)
{	
	int res, state_auth=0, pid;
	int auth_requete, auth_reponse;

  // J'instancie une structure messForm et je l'appel mesg.
  messForm mesg;

  auth_requete = msgget(CLEF_AUTH_REQUETES, 0700 | IPC_CREAT);
  if (auth_requete == -1) { printf("Error : AUTH requete\n"); exit(0); }

  auth_reponse = msgget(CLEF_AUTH_REPONSES, 0700 | IPC_CREAT);
  if (auth_reponse == -1) { printf("Error : AUTH reponse\n"); exit(0); }

	auth.id=getpid();
	auth.action=0;
  getPseudo();

  //Envoi du formulaire d'auth au serveur
  res = msgsnd(auth_requete, & auth, sizeof(auth), 0);
  if (res == -1) { printf("Error: Send AUth");exit(0); }

  //Attente de la reponse
  res = msgrcv(auth_reponse, & state_auth, sizeof(int), 0, 0);
  if (res == -1) { printf("Error: recieve AUth");exit(0); }

  //  Si la connection a échoué, on quitte
  //  Sinnon, on met le message de bienvenue.
  if(state_auth==0){ printf("Echec...\n"); exit(0); }
  else{ printf("Bienvenue !\nIl existe 2 commande:\n-q pour quitter\n-list pour afficher les clients\n"); }
  // On crée un enfant pour recevoire les message privée
  pid_t childID = fork();
  switch(childID) {
    case 0:
      do {
        if(msgrcv(msgget(getppid(),0700 | IPC_CREAT), &mesg, sizeof(messForm), 0, 0) != -1) {
          printf("New message from %s : \n", mesg.pseudo);
          printf("%s\n", mesg.content);
        } else {
          perror("Error when get message from server ");
        }
      } while(1);
      break;
    default:
      saisie();
      break;
  }

}

//Récupère la saisie du pseudo
void getPseudo()
{
	printf("Quel est votre pseudo ? \n");
	scanf("%s", auth.pseudo);
	getchar();
}