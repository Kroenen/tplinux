#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <signal.h>
 
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>

#define TRUE (1==1)
#define FALSE (!TRUE) 
#define CLEF_AUTH_REQUETES         0x00012345
#define CLEF_AUTH_REPONSES         0x00012346
#define CLEF_MESS_REQUETES         0x00012347
#define CLEF_MESS_REPONSES         0x00012348
#define CLEF_PRIRAGE_LIST			 0x00012349
#define CLEF_PRIRAGE_NB			   0x00012350
#define LG_MAX                512

struct authForm
{
	long id;
	int action;
	char pseudo[ LG_MAX ];
} auth;

typedef struct {
	int id;
	char pseudo[ LG_MAX ];
	char content[ LG_MAX ];
} messForm;

typedef struct{
	long id;
	char pseudo[ LG_MAX ];	
}listUsers;

typedef struct 
{
	int user;
} nbUser;

void getPseudo();
void new_mess_tchat(int sig);
void saisie();
void print_client();
void sendTo(char * text, char * from);
char *str_sub (const char *s, unsigned int start, unsigned int end);