// Gérer les message envoyer par le serveur
// Se déclenche quand le serveur nous indique qu'il y a un nouveau messgage.
void new_mess_tchat(int sig)
{	
	// J'instancie une structure messForm et je l'appel mesg.
	messForm mesg;

	int zone_reponse;
	void* mem_partagee;

	zone_reponse =  shmget(CLEF_MESS_REPONSES,100,0700 | IPC_CREAT);
  if (zone_reponse == -1) { printf("Error : zone reponse\n"); }

  if((mem_partagee = shmat(zone_reponse,NULL,0)) == (void*)-1){
  	printf("erreur de lecture \n");
  }

	mesg = *((messForm*)mem_partagee);

	printf("%s a dit : %s\n",mesg.pseudo, mesg.content);
}