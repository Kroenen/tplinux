//Attend la saisie de l'utilisateur si il veux participer au tchat
void saisie()
{
	int mess_requete, auth_requete, res;
	messForm mesg;

  mess_requete = msgget(CLEF_MESS_REQUETES, 0700 | IPC_CREAT);
  if (mess_requete == -1) { printf("Error : AUTH requete\n"); exit(0); }

  auth_requete = msgget(CLEF_AUTH_REQUETES, 0700 | IPC_CREAT);
  if (auth_requete == -1) { printf("Error : AUTH requete\n"); exit(0); }

  // J'attend que l'utilisateur appuis sur ENTRÉE
  do{
    mesg.id=0;
    fgets(mesg.content,LG_MAX,stdin);
    strtok(mesg.content, "\n");       //  J'enlève le \n sinnon c'est pas beau.
    strcpy(mesg.pseudo, auth.pseudo);

    // Si l'utilisateur a saisi "q" cela veux dire qu'il quitte le tchat.
    if(strcmp(mesg.content,"q")==0)
    {
      auth.action=1;
      //Envoi du formulaire d'auth au serveur
      res = msgsnd(auth_requete, & auth, sizeof(auth), 0);
      if (res == -1) { printf("Error: Send AUth");exit(0); }
      exit('O');

    }else{  //  Sinnon, c'est qu'il a tapper une commande.

      // Si la commande ne contien que liste, on lui affiche les utilisateur connecter.
      if((strcmp(mesg.content,"list")==0)||(strcmp(mesg.content,"liste")==0))
      {
        print_client();
        printf("Pour envoyer un message privé a un user, tapez /<pid> \n");
      }else{
        //  Si la commande commence par un /, c'est qu'il veut envoyer un mp
        if(mesg.content[0]==47){
          sendTo(mesg.content,mesg.pseudo);
        }else{
        	//Envoi du message au serveur, on ne se préocupe pas de la suite.
      		res = msgsnd(mess_requete, & mesg, sizeof(mesg), 0);
      		if (res == -1) { printf("Error: Send Mess");exit(0); }      
        }
      }
    }
  }while(TRUE);
}

//Affiche les clients
void print_client()
{
  int zone_reponse1, zone_reponse2;
  listUsers users[20];
  nbUser nb;
  void *mem_partagee1, *mem_partagee2;

  zone_reponse1 =  shmget(CLEF_PRIRAGE_LIST,sizeof(listUsers),0700 | IPC_CREAT);
  if (zone_reponse1 == -1) { printf("Error : zone reponse 2\n"); }

  if((mem_partagee1 = shmat(zone_reponse1,NULL,0)) == (void*)-1){
    printf("erreur de lecture \n");
  }

  zone_reponse2 =  shmget(CLEF_PRIRAGE_NB,sizeof(nbUser),0700 | IPC_CREAT);
  if (zone_reponse2 == -1) { printf("Error : zone reponse 3\n"); }

  if((mem_partagee2 = shmat(zone_reponse2,NULL,0)) == (void*)-1){
    printf("erreur de lecture \n");
  }

  nb = *((nbUser*)mem_partagee2);

  for (int i = 0; i < nb.user; ++i)
  {
    users[i] = ((listUsers*)mem_partagee1)[i];    
    printf("pseudo : %s -- pid : %ld\n", users[i].pseudo, users[i].id);
  }
}

// Envoi un mp a un autre utilisateur en passant par le serveur.
void sendTo(char * text, char * from)
{
  char message[LG_MAX], *temp ;
  int code,res,mess_requete;
  messForm mesg;

  mess_requete = msgget(CLEF_MESS_REQUETES, 0700 | IPC_CREAT);
  if (mess_requete == -1) { printf("Error : AUTH requete\n"); exit(0); }

  //on découpe la chaine pour récupéré le pid et le message du client.
  temp =strchr(text,'/');
  temp++;
  code = atoi(temp);

  printf("Message a envoyer : ");
  fgets(message,LG_MAX,stdin);
  strtok(message, "\n");

  mesg.id=code;
  strcpy(mesg.content,message);
  strcpy(mesg.pseudo, from);

  //Envoi du message au serveur, on ne se préocupe pas de la suite.
  res = msgsnd(mess_requete, & mesg, sizeof(mesg), 0);
  if (res == -1) { printf("Error: Send Mess");exit(0); }  

}