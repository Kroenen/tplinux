#include "serveur/header.h"

#include "serveur/pere.c"
#include "serveur/fils.c"

//Crée un fils pour gerer les messages
void main(void){
	int pid;
	
	pid=fork();

	switch(pid){
		case -1:
			printf("Erreur: echec du fork()\n");
			exit(1);
			break;
		case 0: // Processus fils 
			// Le fils s'occupe du tchat.
			fils();
			break;
		default:
			//	Le père s'occupe des authentification.
			pere(pid);
		break;
	}
}